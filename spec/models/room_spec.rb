require 'rails_helper'

RSpec.describe Room, :type => :model do

  let(:room) { FactoryGirl.build(:room) }
  it { expect(room).to have_many(:tasks) }
  it { expect(room).to allow_value("Polish ąłóźćżś").for(:name) }
  it { expect(room).to validate_presence_of(:name) }



end