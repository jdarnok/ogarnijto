FactoryGirl.define do
  factory :house do
    name "Mieszkanie"
address "al. Pokoju 29/22"
city "Krakow"
    after(:create) { |house| house.users = [FactoryGirl.create(:user)]}
    after(:create) { |house| Factory(:task, :taskable => house) }
  end

end
