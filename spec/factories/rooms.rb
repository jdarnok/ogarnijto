FactoryGirl.define do
  factory :room do
    name "MyString"

    after(:create) { |room| Factory(:task, :taskable=> room) }
  end

end
