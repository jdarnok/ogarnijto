FactoryGirl.define do
  factory :user do

    nick "MyString"
avatar "MyString"
sequence (:email) {|n| "johndoe#{n}@example.com"}
password_digest "MyString"
house_id 1

  after(:create) { |user| user.houses = [FactoryGirl.create(:house)]}
  end

end
