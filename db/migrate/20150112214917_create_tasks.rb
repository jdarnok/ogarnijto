class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.boolean :privacy
      t.date :date
      t.boolean :finished
      t.references :taskable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
