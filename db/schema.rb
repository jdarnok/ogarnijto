# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

<<<<<<< HEAD
ActiveRecord::Schema.define(version: 20150118203502) do
=======
ActiveRecord::Schema.define(version: 20150117170735) do
>>>>>>> d10e1557cd4496ca1ad7d64492253cd421f70446

  create_table "houses", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jobs", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "task_id"
  end

  create_table "payments", force: true do |t|
    t.integer  "task_id"
    t.string   "type"
    t.string   "name"
    t.string   "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rooms", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rooms_users", force: true do |t|
    t.integer "room_id"
    t.integer "user_id"
  end

  add_index "rooms_users", ["room_id"], name: "index_rooms_users_on_room_id"
  add_index "rooms_users", ["user_id"], name: "index_rooms_users_on_user_id"

  create_table "tasks", force: true do |t|
    t.boolean  "private"
    t.date     "date"
    t.boolean  "finished"
    t.integer  "taskable_id"
    t.string   "taskable"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tasks", ["taskable_id"], name: "index_tasks_on_taskable_id"

  create_table "users", force: true do |t|
    t.string   "nick"
    t.string   "avatar"
    t.string   "email"
    t.string   "password_digest"
    t.integer  "house_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "utilities", force: true do |t|
    t.string   "fixed"
    t.string   "variable"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
