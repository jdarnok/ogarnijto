class Task < ActiveRecord::Base
  belongs_to :taskable, polymorphic: true
  has_many :jobs

  validates :privacy, inclusion: { in: [true, false] }
  validates :privacy, exclusion: { in: [nil] }
  validates :date, presence: true

  validates :finished, inclusion: { in: [true, false] }
  validates :finished, exclusion: { in: [nil] }
end
