class Payment < ActiveRecord::Base
  validates :price, presence: true
  validates :type, presence: true
  validates :type, inclusion: { in: %w(fixed variable rent)}
  belongs_to :task
end
