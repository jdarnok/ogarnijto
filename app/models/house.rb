class House < ActiveRecord::Base
  validates :name, presence: true
  # validates :address, presence: true
  has_many :users
  has_many :tasks, as: :taskable
end